// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArenaShooterMainMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ARENASHOOTERGAME_API AArenaShooterMainMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AArenaShooterMainMenuGameMode();

protected:
	UFUNCTION(BlueprintCallable)
		bool CreateHostBeacon();

		class AArenaShooterBeaconHostObject* HostObject{ nullptr };

public:
	UFUNCTION(BlueprintCallable)
		class AArenaShooterBeaconHostObject* GetBeaconHost() { return HostObject; }
};
