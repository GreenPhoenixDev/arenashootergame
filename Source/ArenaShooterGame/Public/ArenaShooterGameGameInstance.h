// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ArenaShooterGameGameInstance.generated.h"

USTRUCT(BlueprintType)
struct FMapInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString MapURL;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText MapName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UTexture2D* MapScreenShot;
};

UCLASS()
class ARENASHOOTERGAME_API UArenaShooterGameGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UArenaShooterGameGameInstance();

protected:
	UFUNCTION(BlueprintCallable)
		void LoadLevel(FString MapURL);
};
