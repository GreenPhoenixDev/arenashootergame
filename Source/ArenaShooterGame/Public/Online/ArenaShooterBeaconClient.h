// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineBeaconClient.h"
#include "ArenaShooterBeaconClient.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FConnectSuccess, bool, D_OnConnected);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDisconnected);

UCLASS()
class ARENASHOOTERGAME_API AArenaShooterBeaconClient : public AOnlineBeaconClient
{
	GENERATED_BODY()
	
public:
	AArenaShooterBeaconClient();

protected:
	UPROPERTY(BlueprintAssignable)
		FConnectSuccess D_OnConnected;

	UPROPERTY(BlueprintAssignable)
		FDisconnected D_OnDisconnected;

protected:
	UFUNCTION(BlueprintCallable)
		bool ConnectToServer(const FString& Address);

	UFUNCTION(BlueprintCallable)
		void LeaveLobby();

	virtual void OnFailure() override;
	virtual void OnConnected() override;

public:
	UFUNCTION(Client, Reliable)
		void Client_OnDisconnected();
	virtual void Client_OnDisconnected_Implementation();
};
