// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineBeaconHostObject.h"
#include "ArenaShooterBeaconHostObject.generated.h"

/**
 * 
 */
UCLASS()
class ARENASHOOTERGAME_API AArenaShooterBeaconHostObject : public AOnlineBeaconHostObject
{
	GENERATED_BODY()
	
public:
	AArenaShooterBeaconHostObject();

protected:
	virtual void OnClientConnected(AOnlineBeaconClient* NewClientActor, UNetConnection* ClientConnection) override;
	virtual void NotifyClientDisconnected(AOnlineBeaconClient* LeavingClientActor) override;
	virtual void DisconnectClient(AOnlineBeaconClient* ClientActor) override;

		void DisconnectAllClients();

public:
	UFUNCTION(BlueprintCallable)
		void ShutDownServer();
};
