// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaShooterGameGameInstance.h"

#include "ArenaShooterGame/Public/ArenaShooterGameGameInstance.h"

UArenaShooterGameGameInstance::UArenaShooterGameGameInstance()
{

}

void UArenaShooterGameGameInstance::LoadLevel(FString MapURL)
{
	GetWorld()->ServerTravel(MapURL);
}